#include "Tunnel.h"
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

/* Libevent. */
#include <event.h>

#define LOGE(...) { fprintf(stderr, __VA_ARGS__); exit(errno); }
#define LOGD(...) { fprintf(stdout, __VA_ARGS__); }

#define MAX_MSG_LEN 4096
struct Msg {
  int len;
  char type;
  char data[MAX_MSG_LEN];
};

void handle_read_server_msg(int fd, short ev, void* arg)
{

}

void handle_write_server_msg(int fd, short ev, void* arg)
{

}

void run()
{
    int server_fd = connect_server("2402:f000:1:4417::900", 5678);
    send_msg(server_fd, 100, 0, NULL);
    struct Msg* p_msg = malloc(sizeof(struct Msg));
    recv_msg(server_fd, p_msg);
    fprintf(stderr, "%s\n", p_msg->data);
    free(msg);
    event_init();
    //struct event_base* ev_base = event_base_new();
    struct event ev_read_server_msg, ev_write_server_msg;
    event_set(&ev_read_server_msg, server_fd, EV_READ | EV_PERSIST, handle_read_server_msg, NULL);
    event_add(&ev_read_server_msg);
    //event_set(&ev_write_server_msg, server_fd, EV_WRITE, handle_write_server_msg, NULL);
    //event_add(&ev_write_server_msg);
    event_dispatch();
    //event_base_dispatch(base);
    return;
}

int connect_server(const char* ip_addr, int port)
{
    int sockfd = socket(PF_INET6. SOCK_STREAM, 0);

    if (-1 == sockfd) {
        LOGE("Failed socket!\n");
    }

    struct sockaddr_in6 addr;
    memset(&addr, 0, sizeof(addr));
    addr.sin6_family = PF_INET6; //AF_INET6
    addr.sin6_port = htons(port);

    if (inet_pton(PF_INET6, ip_addr, &addr.sin6_addr) < 0) {
        LOGE("Failed inet_pton!\n");
    }

    if (connect(sockfd, (struct sockaddr*)&addr, sizeof(addr)) < 0) {
        LOGE("Failed connect %s:%d!\n", ip_addr, port);
    }

    return sockfd;
}



void send_msg(int sockfd, char msg_type, int msg_len, char msg_data)
{
    static struct Msg msg;

    //sanity check
    if (!msg_data) {
        LOGD("Msg is empty!\n")
    }

    if (msg_len > MAX_MSG_LEN) {
        LOGE("Msg too long\n");
    }

    msg.len  = msg_len;
    msg.type = msg_type;
    memcpy(msg.data, msg_data, msg_len);
    //TODO send len
    int send_len = send(sockfd, &msg, sizeof(int) + sizeof(char) + msg.len, 0);
    if (send_len < 0) {
        LOGE("Failed send!\n");
    }
}

void recv_wrap(int sockfd, void* buf, size_t len, int flags)
{
    while (len > 0)
    {
        int recv_len = recv(sockfd, buf, len, flags);
        if (recv_len <= 0) {
            LOGE("Failed recv!\n")
        }
        len -= recv_len;
        buf += recv_len
    }
    return;
}

void recv_msg(int sockfd, struct Msg* p_msg)
{
    recv_wrap(sockfd, &p_msg->len, sizeof(p_msg->len), 0);
    recv_wrap(sockfd, &p_msg->type, sizeof(p_msg->type), 0);
    recv_wrap(sockfd, &p_msg->data, p_msg->len, 0);
    return;
}